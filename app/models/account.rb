class Account < ActiveRecord::Base
  include PublicActivity::Model
  tracked owner: Proc.new { |controller, model| controller.current_user ? controller.current_user : nil }
  
  has_many :cards , dependent: :destroy
  accepts_nested_attributes_for :cards

  def full_name
    "#{first_name} #{last_name}"
  end

end
